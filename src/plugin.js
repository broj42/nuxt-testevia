import Vue from 'vue'
import errorHandler from <%= serialize(options.errorHandler) %>;

let testeviaUrls = [];

export default (context, inject) =>{
  let options = <%= serialize(options) %>;

  context.app.router.afterEach((to, from) => {
    if(!process.server){
      testeviaUrls.unshift(`${from.fullPath} -> ${to.fullPath}`);
      if(testeviaUrls.length > 3){
        testeviaUrls.pop();
      }
    }
  })

  Vue.config.errorHandler = (err) => {
    if(!context.isDev || options.debug){
      console.log(err)
      errorHandler.sendError(process.server ? context.route.fullPath : window.location.href, err, testeviaUrls, options);
    }
  }

  inject('sendError', (err, options) => errorHandler.sendError(err, options))
}
