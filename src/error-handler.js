import axios from 'axios'

function whichBrowser(){
  let ua = navigator.userAgent, tem,
  M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
  if(/trident/i.test(M[1])){
      tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
      return 'IE '+(tem[1] || '');
  }
  if(M[1]=== 'Chrome'){
      tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
      if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
  }
  M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
  if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
  return M.join(' ');
};

function whichOS(){
  if (navigator.appVersion.indexOf('Win')!=-1) return 'Windows';
  if (navigator.appVersion.indexOf('Mac')!=-1) return 'MacOS';
  if (navigator.appVersion.indexOf('X11')!=-1) return 'UNIX/Linux/BSD';
  if (navigator.appVersion.indexOf('Linux')!=-1) return 'Linux';
  return 'Unknown OS'
}

export default {
  errorHandler: function(options){
    let self = this;
    return function errorHandlerMiddleware(err, req, res, next) {
      if (options.project && options.apiKey) self.sendError(req.url, err, options)
      return next(err);
    };
  },

  sendError: function(url, err, urls, options){

    let context = {
      project: options.project,
      source: 'frontend',
      data: {
        url: url
      },
    }

    if(err.message) context.data.error_message = err.message;
    if(err.code) context.data.code = err.code;
    let stack = '\n';
    if(urls) stack = 'Urls: ' + urls.reverse().join('\n') + '\n';

    if(!process.server){
      stack += '\nClient side\n\nDevice: ';
      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        stack += 'Mobile\n';
      } else stack += 'Desktop\n'

      try{
        context.data.browser = whichBrowser() + ' - OS: ' + whichOS();
      } catch(e){

      }

    } else {
      stack += '\nServer side\n'
    }

    if(err.stack) {
      stack += '\nStack: ' + err.stack
    }

    context.data.stacktrace = stack;

    let testeviaUrl = options.apiUrl ? options.apiUrl : 'https://testevia.broj42.com/api/log-errors/';

    axios.post(testeviaUrl, context, {headers: {'Authorization': options.apiKey}})
    .catch(function(e){
      // console.dir(e)
    });
  }
}
