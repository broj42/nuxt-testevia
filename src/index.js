const path = require('path')
const errorHandler = require('./error-handler.js').default

module.exports = function(options) {
  this.addPlugin({
    fileName: 'testevia.js',
    src: path.resolve(__dirname, 'plugin.js'),
    options: {
      ...options,
      errorHandler: path.resolve(__dirname, './error-handler.js')
    }
  });

  this.nuxt.hook('render:errorMiddleware', function(app){app.use(errorHandler.errorHandler(options))})
}

module.exports.meta = require('../package.json')